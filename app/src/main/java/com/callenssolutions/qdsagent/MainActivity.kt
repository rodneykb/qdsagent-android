package com.callenssolutions.qdsagent

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.callenssolutions.qdsagent.base.BaseActivity
import com.callenssolutions.qdsagent.fragments.FragmentPastOrders
import com.callenssolutions.qdsagent.fragments.FragmentStandingOrders
import com.callenssolutions.qdsagent.model.GetDeliveryListResponse
import com.callenssolutions.qdsagent.util.QDSApiEndpoints
import com.google.gson.Gson
import com.orhanobut.logger.Logger
import eu.davidea.flexibleadapter.items.IFlexible
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {

    private val FRAGMENT_CONTAINER_ID = R.id.fragment_container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        //var deliveriesUpcoming : List<IFlexible<RecyclerView.ViewHolder>>  = this.getUpcomingDeliveries()!!
        this.getAllDeliveries()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        configureAndLoadFragments()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishActivity(0)
    }

    private fun configureAndLoadFragments() {
        startFragment(FragmentPastOrders())
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                startFragment(FragmentPastOrders())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                startFragment(FragmentStandingOrders())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun startFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(FRAGMENT_CONTAINER_ID, fragment).commit()
    }

    private fun getUpcomingDeliveries() : List<IFlexible<RecyclerView.ViewHolder>>? {
        return null
    }


    fun getAllDeliveries(){

        AndroidNetworking.get(QDSApiEndpoints.DELIVERIES_URL)
                .build()
                .getAsObject(GetDeliveryListResponse::class.java, object : ParsedRequestListener<GetDeliveryListResponse> {
                    override fun onResponse(response: GetDeliveryListResponse?) {
                        var deliveriesUpcoming : List<IFlexible<RecyclerView.ViewHolder>>

                        Logger.i("***************Response*************")
                        Logger.json(Gson().toJson(response))
                    }

                    override fun onError(error: ANError) {
                        Logger.wtf("Error....\n\n" + Gson().toJson(error)+"\n\n")
                    }

                })
    }


}
