package com.callenssolutions.qdsagent.session

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.callenssolutions.qdsagent.model.SmartFacebookUserCustom
import com.callenssolutions.qdsagent.model.SmartGoogleUserCustom
import com.callenssolutions.qdsagent.model.SmartUserWithKey
import com.google.gson.Gson
import studios.codelight.smartloginlibrary.users.SmartFacebookUser
import studios.codelight.smartloginlibrary.users.SmartGoogleUser
import studios.codelight.smartloginlibrary.util.Constants

class UserSessionManagerCustom {
    /**
     * This static method can be called to get the logged in user.
     * It reads from the shared preferences and builds a SmartUser object and returns it.
     * If no user is logged in it returns null
     */
    public fun getCurrentUser(context: Context): SmartUserWithKey? {
        var smartUser: SmartUserWithKey? = null
        val preferences = context.getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE)
        val gson = Gson()
        val sessionUser = preferences.getString(Constants.USER_SESSION, Constants.DEFAULT_SESSION_VALUE)
        val user_type = preferences.getString(Constants.USER_TYPE, Constants.CUSTOMUSERFLAG)
        if (sessionUser != Constants.DEFAULT_SESSION_VALUE) {
            try {
                when (user_type) {
                    Constants.FACEBOOKFLAG -> smartUser = gson.fromJson(sessionUser, SmartFacebookUserCustom::class.java)
                    Constants.GOOGLEFLAG -> smartUser = gson.fromJson(sessionUser, SmartGoogleUserCustom::class.java)
                    else -> smartUser = gson.fromJson(sessionUser, SmartUserWithKey::class.java)
                }
            } catch (e: Exception) {
                Log.e("GSON", e.message)
            }

        }
        return smartUser
    }

    /**
     * This method sets the session object for the current logged in user.
     * This is called from inside the SmartLoginActivity to save the
     * current logged in user to the shared preferences.
     */
    internal fun setUserSession(context: Context, smartUser: SmartUserWithKey?): Boolean {
        val preferences: SharedPreferences
        val editor: SharedPreferences.Editor
        if (smartUser != null) {
            try {
                preferences = context.getSharedPreferences(Constants.USER_PREFS, Context.MODE_PRIVATE)
                editor = preferences.edit()

                if (smartUser is SmartFacebookUser) {
                    editor.putString(Constants.USER_TYPE, Constants.FACEBOOKFLAG)
                } else if (smartUser is SmartGoogleUser) {
                    editor.putString(Constants.USER_TYPE, Constants.GOOGLEFLAG)
                } else {
                    editor.putString(Constants.USER_TYPE, Constants.CUSTOMUSERFLAG)
                }

                val gson = Gson()
                val sessionUser = gson.toJson(smartUser)
                Log.d("GSON", sessionUser)
                editor.putString(Constants.USER_SESSION, sessionUser)
                editor.apply()
                return true
            } catch (e: Exception) {
                Log.e("Session Error", e.message)
                return false
            }

        } else {
            Log.e("Session Error", "User is null")
            return false
        }
    }
}