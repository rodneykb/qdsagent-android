package com.callenssolutions.qdsagent.util

class QDSApiEndpoints {


    companion object {
        private val BASE_URL = "http://18.191.21.13:8080/qds-api"
        val DELIVERIES_URL = BASE_URL + "/api/get/deliveries?page=0&size=10&sort=requestedAt,desc"

        val AUTH_ENPOINT = BASE_URL + "/auth"
        val AUTH_ENPOINT_WITH_PHONE = BASE_URL + "/auth/verify/passcode"
        val REQUEST_NEW_PASSCODE_ENDPOINT = "$BASE_URL/auth/request/new/passcode"
    }

}