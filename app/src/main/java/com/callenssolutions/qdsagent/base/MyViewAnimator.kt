package com.callenssolutions.qdsagent.base

import android.view.View
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

open class MyViewAnimator : BaseViewAnimator {
    override fun animate(viewToAnimate: View) {
        YoYo.with(Techniques.FadeInRight)
                .duration(700)
                .repeat(0)
                .playOn(viewToAnimate)
    }
}