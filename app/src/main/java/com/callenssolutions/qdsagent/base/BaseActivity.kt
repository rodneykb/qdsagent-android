package com.callenssolutions.qdsagent.base

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity



open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        getPermissions()
    }

    private fun getPermissions(){

    }
}