package com.callenssolutions.qdsagent.base

import android.app.Application
import com.androidnetworking.AndroidNetworking
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger

class BaseApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        initApp()
    }

    fun initApp(){
        AndroidNetworking.initialize(applicationContext)
        Logger.addLogAdapter(AndroidLogAdapter())
    }
}