package com.callenssolutions.qdsagent.base

import android.view.View

interface BaseViewAnimator {
    fun animate(viewToAnimate:View)
}