package com.callenssolutions.qdsagent.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.callenssolutions.qdsagent.R
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import io.swagger.client.model.Delivery

class DeliverySingle(var deliverySingle: Delivery) : AbstractFlexibleItem<DeliverySingle.CustomViewHolder>() {

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, holder: CustomViewHolder?, position: Int, payloads: MutableList<Any>?) {
        holder!!.refNo.text = deliverySingle.referenceNumber.toString()
        holder!!.fromAddress.text = deliverySingle.fromAddress.toString()
        holder!!.toAddress.text = deliverySingle.toAddress.toString()
        holder!!.status.text = deliverySingle.status.toString()
    }

    override fun equals(other: Any?): Boolean {
        return this === other
    }

    override fun createViewHolder(view: View?, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?): CustomViewHolder {
        return CustomViewHolder(view!!, adapter!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.delivery_list_item
    }

    inner class CustomViewHolder(view: View, adapter: FlexibleAdapter<*>) : FlexibleViewHolder(view, adapter) {

        var refNo: TextView = view.findViewById(R.id.ref_no)
        var fromAddress: TextView = view.findViewById(R.id.from_address)
        var toAddress: TextView = view.findViewById(R.id.to_address)
        var status: TextView = view.findViewById(R.id.status)

    }

}