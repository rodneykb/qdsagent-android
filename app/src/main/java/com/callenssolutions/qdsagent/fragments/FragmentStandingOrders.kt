package com.callenssolutions.qdsagent.fragments

import com.callenssolutions.qdsagent.R
import com.callenssolutions.qdsagent.fragments.base.FragmentBase

class FragmentStandingOrders : FragmentBase() {

    override fun getResourceID(): Int {
        return R.layout.fragment_standing_orders
    }

}