package com.callenssolutions.qdsagent.fragments

import com.callenssolutions.qdsagent.R
import com.callenssolutions.qdsagent.fragments.base.FragmentBase

class FragmentPastOrders : FragmentBase() {

    override fun getResourceID(): Int {
        return R.layout.fragment_past_orders
    }
}