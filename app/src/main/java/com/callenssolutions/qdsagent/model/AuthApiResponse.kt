package com.callenssolutions.qdsagent.model

import io.swagger.client.model.User

class AuthApiResponse {
    val token = ""
    var payload: Payload? = null
    val user: User? = null
}