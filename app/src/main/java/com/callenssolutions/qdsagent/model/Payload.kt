package com.callenssolutions.qdsagent.model

import io.swagger.client.model.User

class Payload {
    var token:String = ""
    var user: User? = null
}