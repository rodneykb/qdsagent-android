package com.callenssolutions.qdsagent.model

import studios.codelight.smartloginlibrary.users.SmartUser

open class SmartUserWithKey : SmartUser() {
    var token:String = ""
}