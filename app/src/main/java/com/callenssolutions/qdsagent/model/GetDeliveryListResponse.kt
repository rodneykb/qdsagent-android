package com.callenssolutions.qdsagent.model

import io.swagger.client.model.Delivery

class GetDeliveryListResponse : GeneralResponse() {
     val payLoad: List<Delivery>? = null

}