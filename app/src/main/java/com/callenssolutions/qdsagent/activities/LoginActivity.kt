package com.callenssolutions.qdsagent.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.callenssolutions.qdsagent.MainActivity
import com.callenssolutions.qdsagent.R
import com.callenssolutions.qdsagent.base.BaseActivity
import com.callenssolutions.qdsagent.base.MyViewAnimator
import com.callenssolutions.qdsagent.model.AuthApiResponse
import com.callenssolutions.qdsagent.model.PayloadLoginRequest
import com.callenssolutions.qdsagent.model.SmartUserWithKey
import com.callenssolutions.qdsagent.session.UserSessionManagerCustom
import com.callenssolutions.qdsagent.util.QDSApiEndpoints
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.orhanobut.logger.Logger
import com.tuenti.smsradar.Sms
import com.tuenti.smsradar.SmsListener
import com.tuenti.smsradar.SmsRadar
import kotlinx.android.synthetic.main.activity_login.*
import studios.codelight.smartloginlibrary.*
import studios.codelight.smartloginlibrary.users.SmartUser
import studios.codelight.smartloginlibrary.util.SmartLoginException
import java.util.regex.Pattern




/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : BaseActivity(), SmartLoginCallbacks {

    private var smartLogin: SmartLogin? = null
    private var smartLoginConfig: SmartLoginConfig? = null
    val smartUser = SmartUserWithKey()
    public var responseAuthApiResponse: AuthApiResponse? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initActivity()
        getPermissions(this)
        initSMSInterceptor(this)
        button_next.setOnClickListener {

            if (isPhoneNumber(edittext_username.text.toString())) {
                requestPassword(edittext_username.text.toString())
                layoutVisibilities(View.GONE, View.VISIBLE)
                MyViewAnimator().animate(passcode_layout)

            }
            edittext_username.error = "Phone number incorrect"

        }


    }

    fun initSMSInterceptor(context: Context) {
        SmsRadar.initializeSmsRadarService(context,  object: SmsListener {

			override fun onSmsSent(sms: Sms) {

			}


			override fun onSmsReceived(sms: Sms) {
                if (sms.address.equals("QDS", true)) {
                    var incomingMessage = sms.msg
                    edittext_passcode.setText(getNumberFromString(incomingMessage).trim())
                    login()
                }
			}
		})


    }

    fun getNumberFromString(stringToCheck:String):String{
        var finalString = ""
        for (ch in stringToCheck.toCharArray()) {
            //5
            if (Character.isDigit(ch)) {
                finalString+=ch
            }
        }
        finalString.trim()
        return finalString
    }

    fun getPermissions(activity: Activity){
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.READ_SMS
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {/* ... */
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {/* ... */
                    }
                }).withErrorListener {
                    error -> Log.e("Dexter", "There was an error: " + error.toString())
                }.onSameThread().check()
    }

    fun layoutVisibilities(phoneLayoutView: Int, passcodeLayoutViewValue: Int) {
        phone_number_layout.visibility = phoneLayoutView
        passcode_layout.visibility = passcodeLayoutViewValue
    }

    override fun doCustomSignup(): SmartUser {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLoginSuccess(user: SmartUser?) {
        //Toast.makeText(this, user.toString(), Toast.LENGTH_SHORT).show()
        startNextActivity()
    }

    override fun onLoginFailure(e: SmartLoginException?) {
        Toast.makeText(this, "Error occured", Toast.LENGTH_SHORT).show()
    }

    fun startNextActivity(){
        startActivity(Intent(applicationContext, MainActivity::class.java))
    }

    override fun doCustomLogin(): SmartUser {


        var payloadLoginRequest = PayloadLoginRequest()
        payloadLoginRequest.phone = edittext_username.text.toString()
        payloadLoginRequest.passCode = edittext_passcode.text.toString()

        //jwtAuthenticationRequest.password = editetext_password.text.toString()

        Logger.json(Gson().toJson(payloadLoginRequest))
        AndroidNetworking.post(QDSApiEndpoints.AUTH_ENPOINT_WITH_PHONE)
                .addApplicationJsonBody(payloadLoginRequest)
                .build()
                .getAsObject(AuthApiResponse::class.java, object : ParsedRequestListener<AuthApiResponse> {
                    override fun onResponse(response: AuthApiResponse?) {
                        //responseAuthApiResponse = response
                        smartUser.token = response!!.payload!!.token
                        smartUser.firstName = response!!.payload!!.user!!.name
                        UserSessionManagerCustom().setUserSession(applicationContext, smartUser)


                        Logger.json(Gson().toJson(smartUser))
                        Logger.json(Gson().toJson(response))
                    }

                    override fun onError(error: ANError) {
                        Logger.wtf("Error....\n" + Gson().toJson(error))
                    }

                })





        //Logger.json(Gson().toJson(smartUser))
        return smartUser
    }

    fun requestPassword(phone: String) {

        var payloadLoginRequest = PayloadLoginRequest()
        payloadLoginRequest.phone = phone
        Logger.i("phonenumber =$phone")
        //payloadLoginRequest.passCode = edittext_passcode.text.toString()

        AndroidNetworking.post(QDSApiEndpoints.REQUEST_NEW_PASSCODE_ENDPOINT)
                .addApplicationJsonBody(payloadLoginRequest)
                .build()
                .getAsObject(AuthApiResponse::class.java, object : ParsedRequestListener<AuthApiResponse> {
                    override fun onResponse(response: AuthApiResponse?) {
                        Logger.json(Gson().toJson(response))
                    }

                    override fun onError(error: ANError) {
                        Logger.wtf("Error....\n" + Gson().toJson(error))
                    }

                })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun initActivity() {
        smartLoginConfig = SmartLoginConfig(this, this)
        smartLoginConfig?.facebookAppId = "jhkjhhjhh"
        smartLoginConfig?.facebookPermissions = null
        smartLoginConfig?.googleApiClient = null
        button_loginwithpasscode.setOnClickListener {
            login()
        }
    }

    fun login(){

        smartLogin = SmartLoginFactory.build(LoginType.CustomLogin)
        smartLogin?.login(smartLoginConfig)
    }

    fun isUserLoggedIn() {
        val homeIntent = Intent(this, MainActivity::class.java)

        if (UserSessionManagerCustom().getCurrentUser(this) != null) {
            startActivity(homeIntent)
        }
    }

    fun isPhoneNumber(phoneNumber: String): Boolean {
        val regexStr = "^[+,0-9]*\$" //accept phonenumbers or +
        val p = Pattern.compile(regexStr)
        if (p.matcher(phoneNumber).matches()) {
            return true
        }
        return false
    }


    override fun onStart() {
        super.onStart()
        layoutVisibilities(View.VISIBLE, View.GONE)
        isUserLoggedIn()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (passcode_layout.visibility.equals(View.VISIBLE)) {
            layoutVisibilities(View.VISIBLE, View.GONE)
        }

    }


}
