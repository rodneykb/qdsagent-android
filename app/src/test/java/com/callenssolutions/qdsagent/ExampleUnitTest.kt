package com.callenssolutions.qdsagent

import com.callenssolutions.qdsagent.activities.LoginActivity
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun extractNumbersFromString(){

        var rawString = "Your passcode is 9708"
        var numberVal = LoginActivity().getNumberFromString(rawString)
        ///println(numberVal)
        assertEquals("9708",numberVal)
    }
}
