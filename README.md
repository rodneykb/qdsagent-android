QDSAgent App
-------------

QDSAgent App is  a mobile app to be used by delivery agents to deliver goods to customers.
![Image](assets/login.png?raw=true "Title")


Libraries Used :
* [Android-transition] [1] ,
* [MaterialTextField] [2] ,
* [convalida] [3] <br/>,

[1]:https://github.com/kaichunlin/android-transition
[2]:https://github.com/florent37/MaterialTextField
[3]:https://github.com/WellingtonCosta/convalida